﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperatorio
{
    public abstract class Empleado
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Area { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Localidad { get; set; }
        public bool CrearUnTicket { get; set; }

        public virtual string DevolverDetalle()
        {
            return String.Format("{1}-{2}-{3}", Nombre, Apellido, "Dni:" + DNI);
        }
        public abstract bool CrearTicket(Ticket tipoticket);
        
    }
}
