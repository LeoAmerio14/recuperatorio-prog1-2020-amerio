﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Recuperatorio
{
    public class Desarrollador : Empleado
    {
        public enum Nivel { Poco = 1, Bastante = 2, Mucho = 3 }
        public Nivel NivelExperiencia { get; set; }
        public Lider LiderACargo { get; set; }

        //CORRECCIÓN: SI EN EL METODO BASE YA COLOCASTE EL Nombre, Apellido y DNI, NO DEBES HACERLO DE NUEVO
        // ESTO SERÍA:  return base.DevolverDetalle() + $" - Nombre de Lider: {LiderACargo.Nombre}";
        public override string DevolverDetalle()
        {
            return base.DevolverDetalle() + $"Nombre: {Nombre} Apellido: {Apellido}, DNI: {DNI} - Nombre de Lider: {LiderACargo.Nombre}";
        }

        public override bool CrearTicket(Ticket tipoticket)
        {
            if ((int)tipoticket.TipoTicket == 2)
            {
                if ((int)LiderACargo.AreaTrabajo == 1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
