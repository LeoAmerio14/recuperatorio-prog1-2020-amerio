﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperatorio
{
    public class Ticket
    {
        public int Codigo { get; set; }
        //COMENTARIO: LOS ENUMERADORES POR LO GENERAL SE COLOCAN TODOS JUNTOS Y ANTES QUE LAS PROPIEDADES PARA DARLE MAS ORGANIZACIÓN AL CÓDIGO.
        public enum Tipo { GestionPermiso = 1, AccesoBaseDatos = 2, AltaUsuario = 3 }
        public Tipo TipoTicket { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Empleado Datos { get; set; }
        public enum Estado { Abierto = 1, EnProceso = 2, Resuelto = 3 }
        public Estado CondicionTicket { get; set; }
        public string Comentario { get; set; }

    }
}
