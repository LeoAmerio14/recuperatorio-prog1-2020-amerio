﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperatorio
{
    public class Principal
    {
        public List<Ticket> Tickets { get; set; }

        //CORRECCIÓN: SOLO SE DEBÍA PASAR UN VALOR NÚMERICO, NO UN TICKET.
        public void ActualizarEstado(Ticket codigo, int nuevoEstado, string comentario)
        {
            foreach (var ticket in Tickets)
            {
                if (ticket.Codigo == codigo.Codigo)
                {
                    // FALTAN VALIDACIONES DEL SENTIDO: NO SE PUEDE PONER EN EnProceso SI EL ESTADO NO ES Abierto
                    ticket.CondicionTicket = (Ticket.Estado)nuevoEstado;
                    ticket.Comentario = comentario;
                }
            }
        }
    }
}
