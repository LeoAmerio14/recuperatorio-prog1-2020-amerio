﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperatorio
{
    public class Lider : Empleado
    {
        public enum AreaAcargo { Desarrollo = 1, Administracion = 2 }
        public AreaAcargo AreaTrabajo { get; set; }

        //CORRECCIÓN: SI EN EL METODO BASE YA COLOCASTE EL Nombre, Apellido y DNI, NO DEBES HACERLO DE NUEVO
        // ESTO SERÍA:  return base.DevolverDetalle();
        public override string DevolverDetalle()
        {
            return base.DevolverDetalle() + $"Nombre: {Nombre} Apellido: {Apellido} DNI: {DNI}";

        }
        public override bool CrearTicket(Ticket tipoticket)
        {
            if ((int)tipoticket.TipoTicket == 1 || (int)tipoticket.TipoTicket == 3)
            {
                return true; 
            }
            return false;
        }
    }
}
